(function () {

    var peer = null; // own peer object
    var conn = [];
    var contacts = [];
    var call = [];
    var rooms = ["room1", "room2", "room3"];
    const key = "fiKz3Hy7aQmc1091I9cS1ImSa5QlxZp812T01p";
    var status = document.getElementById("status");
    const video = document.createElement('video');
    const video2 = document.createElement('video');

    //Gets user microphone
    mediaStream = new MediaStream();


    function initialize() {
        // Create own peer object
        peer = new Peer(key , {
            debug: 2
        });

        peer.on('open', function (id) {
            // Workaround for peer.reconnect deleting previous id
            if (peer.id === null) {
                console.log('Received null id from peer open');
                peer.id = key;
            }

            console.log('ID: ' + peer.id);
            status.innerHTML = "Awaiting connection...";
        });

        peer.on('connection', function (c) {
            //Check the fist free array entry
            var count = 0;
            while (conn[count] != null) {
                count++;
            }
            conn[count] = c;
            contacts[count] = c.peer;
            console.log("Connected with slot: " + count + " to: " + conn[count].peer);
            status.innerHTML = status.innerHTML + "<br/>Connected with slot: " + count + " to: " + conn[count].peer;
            dataHandler(count);
            //sendRooms(count);
            count = 0;
        });

        peer.on('disconnected', function () {          
            console.log('Connection lost. Please reconnect');
            status.innerHTML = status.innerHTML + "<br/>Connection lost. Please reconnect";

            // Workaround for peer.reconnect deleting id
            peer.id = key;
            peer._lastServerId = key;
            peer.reconnect();
        });

        //Closing connection
        peer.on('close', function() {
            conn = null;
            console.log('Connection destroyed');
            status.innerHTML = status.innerHTML + "<br/>Connection destroyed. Please refresh";
        });

        //Answer the call
        peer.on('call', function(calll) {               
            status.innerHTML = status.innerHTML + "<br/>Receiving incoming call";
            var callcount = 0;
            while (call[callcount] != null) {
                callcount++;
            }

            call[callcount] = calll;
            answerAudioCall(callcount);
        });

        //Errors report
        peer.on('error', function (err) {
            console.log(err);
            alert('' + err);
        });
    }


    /**
     * Handles data and calls
     */
    function dataHandler(count) {

        /*conn.on('open', function () {
            status.innerHTML = "Connected to: " + conn.peer;
            console.log("Connected to: " + conn.peer);

            // Check URL params for comamnds that should be sent immediately
            var command = getUrlParam("command");
            if (command)
                conn.send(command);
        });*/

        //Closes connection
        conn[count].on('close', function () {
            status.innerHTML = status.innerHTML + "<br/>Peer disconnected: " + conn[count].peer;
            conn[count] = null;
            contacts[count] = null;
            return;
        });
        
        // Handle incoming data (messages only)
        conn[count].on('data', function (data) {
            if (data == "/sendcontacts") {
                var temp = [];
                contacts.forEach(element => {
                    if (element != conn[count].peer)
                    temp.push(element);
                });
                conn[count].send(temp);
            } else {
                console.log("data received:" + data);
                console.log("data received from:" + conn[count].peer);
                sendData(conn[count].peer, data);
            }
        });  

        //Checks if the call is closed
        call.forEach(element => {
            if (element != null) {
                element.on('close', function() {
                    status.innerHTML = status.innerHTML + "<br/>Call closed: " + element.peer;
                });
            }  
        });                   
    }

    /**
     * Sends the rooms to the client
     * @param {*} count 
     */
    function sendRooms(count) {
        rooms.forEach(element => {
            conn[count].send("/roomdata " + element);
            console.log("sent: " + element);
        });
    }

    /**
     * Sends data to the connected clients
     * @param {*} senderId 
     * @param {*} data 
     */
    function sendData(senderId, data) {
        conn.forEach(element => {
            if (element.peer != senderId) {
                element.send(data);
                console.log("data sent");
            }
        });
    }

    /**
     * Answers an incoming call with audio only
     * @param {*} id 
     */
    function answerAudioCall(id) {

        try {
            mediaStream = video.srcObject;
        } catch (err){
            console.log(err.name + ": " + err.message);
        }
        call[id].answer(mediaStream);
        console.log("Call: " + call[id]);
        console.log("id: " + id);
        //test
        /*const video = document.createElement('video');
        video.srcObject = mediaStream;
        video.play();*/
        
        receivePeerVideoForAnswer(id);
    }

    /**
     * Receves the other peer media stream for the answerer
     * @param {*} id 
     */
   function receivePeerVideoForAnswer(id) {
    call[id].on('stream', function(stream) {
        // `stream` is the MediaStream of the remote peer.
        /*stream.getAudioTracks().forEach(element => {
            mediaStream.addTrack(element);
        });*/
        //mediaStream = stream;
        video.srcObject = stream;
        //test
        /*const video = document.createElement('video');
        video.srcObject = mediaStream;
        video.play();*/

        //console.log("audio track: " + mediaStream.getAudioTracks());
        /*videoin.srcObject = stream;
        videoin.onloadedmetadata = function(e) {
        videoin.play();
        }*/
    });
}

    // Since all callbacks are setup, start the process of obtaining an ID
    initialize();
})();