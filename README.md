# Peer to Peer WebRTC System with PeerJS #

This system was designed to allow end to end communication between two peers but also, using the same mechanism, between many peers.
Included in this repository there are the client and the server and their dependencies (excluding peerjs.min.js, bootstrap.min.css and adapter-latest.js that are fetched from their official online servers). 

### Client ###
- client.html
- client.js
- client.css

### Server ###
- server.html
- server.js
- server.css

## Setup ##

Right now the whole system relies on the PeerJS test server so there’s no signaling server to setup but server.html is required for group chat, audio conference and video conference (the last one not working right now).

## Usage ##

### Client ###

1. Open client.html with a web browser (preferably Chrome)
2. Fill the ID textbox with the ID of another client and click “connect” (if the client with the specified ID does not exist it will return an error)

The status box will tell every event that’s happening.
Now you can use the textual chat on the right with the connected client to write messages  and you can call it or video call it.
The client receiving the call will see two new buttons appearing allowing it to answer the call with the microphone only or with the webcam and microphone.
Then both clients will see the “close call” button to end the call.
Right now it’s not possible to refuse the call (To be fixed in future).

WARNING: The “disconnect” button will disconnect the client/server you’re connected with, but will not end any ongoing call/videocall. To do that you must close the call with the dedicated button.

When you want to disconnect from the other client you can click the “disconnect” button.

### Server (optional)  ###

1. Open server.html with a web browser (preferably Chrome)

WARNING: There can only be a single instance of the server. So if you try to open the server and another person has it opened, you will not be able to connect and will return an error.

INFO: The server processes only the messages to deliver them to the connected clients. The audio/video calls are always client to client so the server serves only the purpose of retrieving all the clients connected and sending the connection info to the requester.

Once the server is opened the clients that want to connect to it can click the “connect to server” button without specifying any receiving ID and connect to the server.

WARNING: Once you click the “connect to server” button you will be able to receive any incoming call coming from the other connected clients without any prompt to answer, that’s because all incoming calls are answered automatically. This is to be fixed in future.

Once the client is connected to the server you should click the “Call Peers on Server” button to ensure you are connected with every other client.

## Code Implementation ##

### Client ###

#### Initialization ####

First of all you need to create your peer object:
```javascript
peer = new Peer();
```
This allows you to execute some code, if you need, right when your peer creation is called and it has returned successfully:
```javascript
peer.on('open', function (id) { ... });
```
This is fired every time there’s a request for a connection coming from the web. The parameter is the connection object:
```javascript
peer.on('connection', function (c) { ... });
```
This allows you to try to connect to the designated peer ID:
```javascript
conn = peer.connect(id , { ... });
```

There are other types of event like: disconnect, close, error, etc. 
For more information on those you can check the official documentation: [here](https://peerjs.com/docs.html#peeron).

#### Connection ####

Once the connection is established you can start to listen for events coming from the connected client.
This allows you to listen for incoming data (it can be any type of data, including objects, strings, and blobs):
```javascript
conn.on('data', function (data) { ... });
```
This allows you to send data to the connected client:
```javascript
conn.send(data);
```
This allows you to listen for incoming calls (audio/video). The parameter is the object containing the information about the connection:
```javascript
peer.on('call', function(call) { ... });
```
This allows you to call the specified peer ID. You also need to provide your Media Stream, otherwise this will be a one way call:
```javascript
call = peer.call(id, mediaStream);
```
This allows you to answer an incoming call. You also need to provide your Media Stream, otherwise this will be a one way call:
```javascript
call.answer(mediaStream);
```
Once a call is answered by the other client you must handle the Media Stream it is sending:
```javascript
call.on('stream', function(stream) { ... });
```
This allows you to close a call:
```javascript
call.close();
```
This is a function I created in order to make conference calls possible.
The client requests the list of clients connected to the server but itself then it stores them in an array called “contacts”.
After that the client proceeds to call every contact he does not have a connection with:
```javascript
function callContacts() {
    var count = 0;
    contacts.forEach(contact => { //contacts is a list of clients connected to the server
        var checkcall = true;
        callserver.forEach(callelement => { //callserver is an array of active connections to other clients
            //here it checks if there's already a connection between this client and another
            if (callelement.peer == contact) {
                checkcall = false;
            }
        });
        // if the client is not yet connected to the other then it proceeds to find an open slot in the call array to store the connection object
        if (checkcall) {
            while (callserver[count] != null) {
                count++;
            }
            callserver[count] = peer.call(contact, selfvideo.srcObject); //here it makes the call passing the audio/video stream to the other client
            console.log("calling: " + contact);
            receivePeerVideoForServerAnswer(count);
        }
    });
}
```

More information about the Data Connection can be found: [here](https://peerjs.com/docs.html#dataconnection).

More information about the Media Connection can be found: [here](https://peerjs.com/docs.html#mediaconnection).

In order to modulate the audio/video stream it's necessary to modify the sdp (Session Description Protocol) when calling the call/answer method.
There are a lot of different parameters you can change if you want to make fine adjustments.
```
Session description
    v=  (protocol version number, currently only 0)
    o=  (originator and session identifier : username, id, version number, network address)
    s=  (session name : mandatory with at least one UTF-8-encoded character)
    i=* (session title or short information)
    u=* (URI of description)
    e=* (zero or more email address with optional name of contacts)
    p=* (zero or more phone number with optional name of contacts)
    c=* (connection information—not required if included in all media)
    b=* (zero or more bandwidth information lines)
    One or more Time descriptions ("t=" and "r=" lines; see below)
    z=* (time zone adjustments)
    k=* (encryption key)
    a=* (zero or more session attribute lines)
    Zero or more Media descriptions (each one starting by an "m=" line; see below)
```
```
Time description (mandatory)
    t=  (time the session is active)
    r=* (zero or more repeat times)
```
```
Media description (optional)
    m=  (media name and transport address)
    i=* (media title or information field)
    c=* (connection information — optional if included at session level)
    b=* (zero or more bandwidth information lines)
    k=* (encryption key)
    a=* (zero or more media attribute lines — overriding the Session attribute lines)
```
Optional values are specified with *.

There are multiple codec choices for both audio and video. By default it's Opus for audio and VP8 for video. Both of them have their own parameters adjustment.

You can check the RFC for Opus [here](https://tools.ietf.org/html/rfc7587).

You can check the RFC for VP8 [here](https://tools.ietf.org/html/rfc7741).

In PeerJS in order to modify the sdp before calling/answering you need to invoke the method sdpTransform when calling the call/answer method. This is an example:
```javascript
call = peer.call(id, mediaStream, {sdpTransform: (sdp) => { 
    return updateBandwidthRestriction(sdp, bandwidth); //this function modifies the sdp with the desired bandwidth in kbs
}});
```
The function updateBandwidthRestriction in implemented as follows:
```javascript
function updateBandwidthRestriction(sdp, bandwidth) {
    let modifier = 'AS';
    //fix for firefox
    if (adapter.browserDetails.browser === 'firefox') {
        bandwidth = (bandwidth >>> 0) * 1000;
        modifier = 'TIAS';
    }
    if (sdp.indexOf('b=' + modifier + ':') === -1) {
        //inserts bandwidth value when non existent
        sdp = sdp.replace(/c=IN (.*)\r\n/g, 'c=IN $1\r\nb=' + modifier + ':' + bandwidth + '\r\n');
    } else {
        //replaces the value with the desired bandwidth
        sdp = sdp.replace(new RegExp('b=' + modifier + ':.*\r\n'), 'b=' + modifier + ':' + bandwidth + '\r\n');
    }
    return sdp;
}
```

To debug/check the effectiveness of the modified sdp you can use the built-in tool on Google Chrome here: [chrome://webrtc-internals/](chrome://webrtc-internals/).

### Server ###

The server function is very simple, in fact the only thing it does is analyse the received data and proceed as follows:

- If it’s a command the server will execute it (the only command available for now is the “/sendcontacts” which provides the requester with the list of connected clients except the one requesting this list. The client will then proceed to call every peer present in that list). 
- Otherwise if it’s a chat message the server will send the message to the connected clients but the sender.

At the end of the day the server is just a client with some specific functionalities because it implements the same calls as a normal client.

## PeerJS vs SIP ##

PeerJS and SIP do the same thing which is to encapsulate the framework WebRTC to offer the user an easier framework to work with.
They obviously use different classes and different methods to accomplish the same goals, but SIP does some extra things that PeerJS does not.
For example SIP can put on hold a call or refuse it while PeerJS can only answer a call.
SIP even offers an authentication to users in order to offer more security while on PeerJS you must implement it yourself.
Unfortunately neither of them offer support for conference calls so in order to do that it’s necessary to do some workarounds.
In the end all those things can be implemented with PeerJS with some work.

## DeepStream ##

DeepStream is another wrapper of WebRTC that could be useful for implementing conference calls, but actually isn't in our case. That’s because it does not provide any transparent method to call multiple peers.

## MCU and SFU ##

MCU (Multipoint Control Unit) is a technology that allows the server to receive multiple streams and mix them together to create a new stream in order to reduce the bandwidth load on the clients during conference calls.
This obviously comes with a cost and that is the computational power needed to make such a mix of streams.
However there is an alternative and that is SFU (Selective Forwarding Unit) that does not mix any streams but just forwards them, when desired, to the other clients.
This technology allows to forward the stream of a client to the other clients by just uploading the stream once.
The server also benefits from it because it does not need much computational power.
The only downside is that the connected clients will receive a number of streams equal to the number of clients streaming with is more bandwidth heavy compared to MCU.
In the end mixing streams together isn’t such a great idea so using SFU is preferred.

There is a promising open source SFU project at [https://github.com/versatica/mediasoup/](https://github.com/versatica/mediasoup/).
